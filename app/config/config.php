<?php
/**
 * Define Root folder
 * Define URL
 * Define Sitename
 */

define('APPROOT', dirname(dirname(__FILE__)) );
define('URLROOT', 'http://oop.oo');
define('SITENAME', 'OOP');

/**
 * DB Params
 */

define('DB_HOST', 'localhost');
define('DB_USER', 'YOUR_DB_USER');
define('DB_PASSWORD', 'YOUR_DB_PASSWORD');
define('DB_NAME', 'YOUR_DB_NAME');