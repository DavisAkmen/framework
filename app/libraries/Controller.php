<?php
/**
 * Base Controller loads model and view
 */

class Controller {
    /**
     * Load model
     */
    public function model($model)
    {
        /**
         * Require model
         */
        require_once '../app/models/'.$model.'.php';
        /**
         * Instantiate model
         */
        return new $model();
    }

    /**
     * Load View
     */

    public function view($view, $data = [])
    {
        if(file_exists('../app/views/'.$view.'.php'))
        {
            require_once '../app/views/'.$view.'.php';
        }else{
            die('View does not exist');
        }
    }
}
